package com.ice.feedreader.rss;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class RSSDatabaseHandler extends SQLiteOpenHelper{
	// Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "rssReader";
 
    // Contacts table name
    private static final String TABLE_RSS = "items";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_LINK = "link";
    private static final String KEY_PUB_DATE = "pub_date";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_GUID = "guid";
    private static final String KEY_READ = "read";
 
    public RSSDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_RSS_TABLE = "CREATE TABLE " + TABLE_RSS + "(" + KEY_ID
                + " INTEGER PRIMARY KEY AUTO INCREMENT," + KEY_TITLE + " TEXT," + KEY_LINK
                + " TEXT," + KEY_PUB_DATE + " TEXT," + KEY_DESCRIPTION
                + " TEXT," + KEY_GUID + " TEXT," + KEY_READ + " TEXT" + ")";
        db.execSQL(CREATE_RSS_TABLE);
    }
 
    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RSS);
 
        // Create tables again
        onCreate(db);
    }
 
    /**
     * Adding a new website in websites table Function will check if a site
     * already existed in database. If existed will update the old one else
     * creates a new row
     * */
    public void addItem(RSSItem item) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, item.getTitle()); // site title
        values.put(KEY_LINK, item.getLink()); // site url
        values.put(KEY_GUID, item.getGuid()); // guid
        values.put(KEY_DESCRIPTION, item.getDescription()); // site description
        values.put(KEY_PUB_DATE, item.getPubdate());
        values.put(KEY_READ, item.isRead());
 
        // Check if row already existed in database
        if (!itemExists(db, item.getGuid())) {
            // site not existed, create a new row
            db.insert(TABLE_RSS, null, values);
            db.close();
        } else {
            // site already existed update the row
            updateItem(item);
            db.close();
        }
    }
 
    /**
     * Reading all rows from database
     * */
    public List<RSSItem> getAllItems() {
        List<RSSItem> itemList = new ArrayList<RSSItem>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_RSS
                + " ORDER BY id DESC";
 
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
            	RSSItem item = new RSSItem();
                
                item.setTitle(cursor.getString(1));
                item.setLink(cursor.getString(2));
                item.setGuid(cursor.getString(5));
                item.setPubdate(cursor.getString(3));
                item.setRead(Boolean.parseBoolean(cursor.getString(6)));
                item.setDescription(cursor.getString(4));
                // Adding contact to list
                itemList.add(item);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
 
        // return contact list
        return itemList;
    }
 
    /**
     * Updating a single row row will be identified by rss link
     * */
    public int updateItem(RSSItem item) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, item.getTitle());
        values.put(KEY_LINK, item.getLink());
        values.put(KEY_GUID, item.getGuid());
        values.put(KEY_DESCRIPTION, item.getDescription());
        values.put(KEY_PUB_DATE, item.getPubdate());
        values.put(KEY_READ, item.isRead());
 
        // updating row return
        int update = db.update(TABLE_RSS, values, KEY_GUID + " = ?",
                new String[] { String.valueOf(item.getGuid()) });
        db.close();
        return update;
 
    }
 
    /**
     * Reading a row (website) row is identified by row id
     * */
    public RSSItem getItem(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
 
        Cursor cursor = db.query(TABLE_RSS, new String[] { KEY_ID, KEY_TITLE,
                KEY_LINK, KEY_GUID, KEY_DESCRIPTION, KEY_PUB_DATE, KEY_READ }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
 
        RSSItem item = new RSSItem(cursor.getString(1), cursor.getString(2),
                cursor.getString(4), cursor.getString(5), cursor.getString(3), Boolean.parseBoolean(cursor.getString(6)));
 
//        item.setGuid((Integer.parseInt(cursor.getString(0)));
//        item.setTitle(cursor.getString(1));
//        item.setLink(cursor.getString(2));
//        item.setRSSLink(cursor.getString(3));
//        item.setDescription(cursor.getString(4));
        cursor.close();
        db.close();
        return item;
    }
 
    /**
     * Deleting single row
     * */
    public void deleteItem(RSSItem item) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RSS, KEY_GUID + " = ?",
                new String[] { item.getGuid()});
        db.close();
    }
 
    /**
     * Checking whether a site is already existed check is done by matching rss
     * link
     * */
    public boolean itemExists(SQLiteDatabase db, String rss_link) {
 
        Cursor cursor = db.rawQuery("SELECT 1 FROM " + TABLE_RSS
                + " WHERE rss_link = '" + rss_link + "'", new String[] {});
        boolean exists = (cursor.getCount() > 0);
        return exists;
    }
}
